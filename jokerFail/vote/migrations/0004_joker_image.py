# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vote', '0003_auto_20151110_0738'),
    ]

    operations = [
        migrations.AddField(
            model_name='joker',
            name='image',
            field=models.FileField(upload_to='images/', default='images/not-img.jpg'),
        ),
    ]
