var babelify = require('babelify');
var browserify = require('browserify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var less = require('gulp-less');
var concat = require('gulp-concat');

function jsBundle(options) {
  return browserify(options.index)
    .transform(babelify)
    .bundle()
    .pipe(source(options.output.file))
    .pipe(gulp.dest(options.output.dir));
}

gulp.task('js', function() {
  return jsBundle({
    index: 'js/app.js',
    output: {
      file: 'main.js',
      dir: 'dist/js/'
    }
  });
});

gulp.task('less', function () {
  return gulp.src(['./less/**/*.less',
        'node_modules/angular-material/angular-material.min.css'])
    .pipe(less())
    .pipe(concat('main.css'))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('copy-html', function() {
    return gulp.src('./template/**/*.html').pipe(gulp.dest('./dist/'));
});

gulp.task('copy-icons', function() {
    return gulp.src(['./icons/**/*.svg','./icons/**/*.ico']).pipe(gulp.dest('./dist/icons/'));
});

gulp.task('build', ['js', 'less', 'copy-html', 'copy-icons']);
