describe('vote-service', function() {

    var Vote ,$httpBackend;

    beforeEach(module('jokerFailApp'));

    beforeEach(inject(function(_$httpBackend_, _Vote_) {
       $httpBackend = _$httpBackend_;
       Vote = _Vote_;
   }));

   it('should vote success', function() {
       var voteFrom = 'ben';
       var voteTo = 'vee';
       $httpBackend.expect(
           'PUT',
           'http://localhost:8081/go/vote/vote',
           {
               from: voteFrom,
               to:voteTo
           })
            .respond({});

       Vote.vote(voteFrom, voteTo);
       $httpBackend.flush();

    });

 });
