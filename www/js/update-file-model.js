let updateFileModel = ['$cookies', '$parse', '$http',
    function ($cookies, $parse, $http) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                let model = $parse(attrs.updateFileModel);
                let modelSetter = model.assign;

                let getFormData = (file, name) => {
                    let fd = new FormData();
                    fd.append('name', name);
                    fd.append('img', file);
                    return fd;
                };

                let updateImage = (file, name) => {
                    let data = getFormData(file, name);
                    //.post('http://localhost:8082/vote/register/', {name: 'ben'}
                    return $http({
                            method  : 'POST',
                            url     : 'http://192.168.1.100:8082/vote/changeimg/',
                            transformRequest: angular.identity,
                            data    : data,
                            headers: {'Content-Type': undefined}
                         })
                        .then((response) => {
                            location.reload();
                            return response.data;
                        });
                };

                element.bind('change', function(e){
                    updateImage(e.target.files[0], $cookies.getObject('joker-ck').name);
                });
            }
        };
    }];

export {
    updateFileModel
}
