let Vote =  ['$log', '$rootScope', '$http',
    function($log, $rootScope, $http) {

        $log.log('[Vote] Start.');

        let vote,
            load;

        vote = (voteFrom, voteto) => {
            return $http.put('vote/vote/', {from: voteFrom, to: voteto})
                .then((response) => {
                    return response.data;
                });
        };

        load = () => {
            return $http.get('vote/')
                .then((response) => {
                    return response.data;
                });
        };

        $log.log('[Vote] End.');

        return {
            vote,
            load
        };

    }];

export {
    Vote
}
