let voteController = ['$scope', '$log', 'Vote', '$cookies',
    function($scope, $log, Vote, $cookies){

        $log.log('[voteController] Start.');

        let ctrl = this;

        let countSecord = 0;

        let memPointJokers = [];

        let memPointSuperJokers = [];

        ctrl.from = false;

        ctrl.superJokers = [];
        ctrl.superJokersMem = [];

        ctrl.jokers = [];
        ctrl.jokersMem = [];

        ctrl.jokerName = '';

        ctrl.value = '';

        ctrl.vote = (joker, voteFrom, voteTo) => {
            if(joker.style[0] === 'p') {
                randomPoint(joker, memPointJokers, 25, 'point-', true);
            } else {
                //randomPoint(joker, memPointSuperJokers, 5, 'angle-', true);
                ctrl.superJokers = randomPointSuperJokers();
            }
            return Vote.vote(voteFrom, voteTo)
                .then((response) => {
                    $log.info('vote result: ' + response);
                    ctrl.value = response;
                    return response;
                });
        };

        ctrl.load = () => {
            ctrl.jokerName = $cookies.getObject('joker-ck').name;

            return Vote.load()
                .then((response) => {
                    //$log.info('load result: ' , JSON.parse(response.jokers));
                    //$log.info('load result: ' , countSecord);
                    countSecord++;
                    if(!angular.equals(ctrl.superJokersMem, JSON.parse(response['super-jokers']))) {
                        ctrl.superJokersMem = JSON.parse(response['super-jokers']);
                        ctrl.superJokers = JSON.parse(response['super-jokers']);
                        ctrl.superJokers = randomPointSuperJokers();
                    } else if(countSecord === 12) {
                        ctrl.superJokers = randomPointSuperJokers();
                    }
                    if(!angular.equals(ctrl.jokersMem, JSON.parse(response.jokers))) {
                        ctrl.jokersMem = JSON.parse(response.jokers);
                        ctrl.jokers = JSON.parse(response.jokers);
                        ctrl.jokers = randomPointJokers();
                    } else if(countSecord === 12) {
                        ctrl.jokers = randomPointJokers();
                        countSecord = 0;
                    }

                    return response;
                });

                //fields.image // .score
                //pk == name
        };

        ctrl.setMove = (jokers) => {
            for(let index in jokers) {
                ctrl.checkMove(jokers[index]);
            }
            return jokers;
        }

        let random = (count) => {
            return Math.floor((Math.random() * count) + 1);
        };

        let randomPoint = (joker, memPoint, max, type, click) => {

            if(click) {
                let numberOld = Number(joker.style.split('-')[1]);
                memPoint[memPoint.indexOf(numberOld)] = 0;
            }

            let number = random(max);
            while(memPoint.indexOf(number) > -1) {
                number++;
                if(number > max) {
                    number = 1;
                }
            }
            memPoint.push(number);
            joker.style = type + number;
        };

        let randomPointSuperJokers = () => {
            memPointSuperJokers = [];
            let jokers = angular.copy(ctrl.superJokers);
            for(let index in jokers) {
                randomPoint(jokers[index], memPointSuperJokers, 4, 'angle-', false);
            }
            return jokers;
        };

        let randomPointJokers = () => {
            memPointJokers = [];
            let jokers = angular.copy(ctrl.jokers);
            for(let index in jokers) {
                randomPoint(jokers[index], memPointJokers, 25, 'point-', false);
            }
            return jokers;
        };

        ctrl.load();
        setInterval(function(){
            ctrl.load();
        }, 5000);

        $log.log('[voteController] End.');

    }];

export {
    voteController
}
