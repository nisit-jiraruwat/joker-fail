import angular from 'angular';

let register = angular.module('register', []);

import {Register} from './register-service';
register.factory('Register', Register);

import {registerController} from './register-controller';
register.controller('registerController', registerController);
