import angular from 'angular';
import 'angular-cookies';

let login = angular.module('login', ['ngCookies']);

import {Login} from './login-service';
login.factory('Login', Login);

import {loginController} from './login-controller';
login.controller('loginController', loginController);
