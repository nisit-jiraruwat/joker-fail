let Login =  ['$log', '$rootScope', '$http',
    function($log, $rootScope, $http) {

        $log.log('[Register] Start.');

        let login;

        login = (name) => {
            return $http.post('vote/login/', {name: name})
                .then((response) => {
                    return response.data;
                });
        };

        return {
            login
        };

    }];

export {
    Login
}
