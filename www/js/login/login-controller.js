let loginController = ['$log', 'Login', '$state', '$cookies',
    function($log, Login, $state, $cookies){

        $log.log('[loginController] Start.');

        let ctrl = this;

        ctrl.name = '';

        ctrl.login = (name) => {
            return Login.login(name)
                .then((response) => {
                    $cookies.putObject('joker-ck', {
                        name: response.name
                    });
                    $state.go('vote');
                    return response;
                });
        };

        ctrl.register = () => {
            $state.go('register');
        };

        $log.log('[loginController] End.');

    }];

export {
    loginController
}
